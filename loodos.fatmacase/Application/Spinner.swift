//
//  Spinner.swift
//  loodos.fatmacase
//
//  Created by Fatma Demirci on 1.06.2020.
//  Copyright © 2020 fatma.demirci. All rights reserved.
//

import NVActivityIndicatorView

open class Spinner {
    
    public static let activityData = ActivityData()
    
    public static func spin() {
        NVActivityIndicatorView.DEFAULT_TYPE = .ballScaleRippleMultiple
        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.cyan
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }
    
    public static func stop() {
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
