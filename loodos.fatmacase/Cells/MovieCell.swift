//
//  MovieCell.swift
//  loodos.fatmacase
//
//  Created by Fatma Demirci on 31.05.2020.
//  Copyright © 2020 fatma.demirci. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet var content_image: UIImageView!
    @IBOutlet var content_title: UILabel!
}
